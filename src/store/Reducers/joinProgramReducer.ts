import { TOGGLE_SELECT_TC, TC_FAIL } from "../constants";

export interface JoinProgramState {
  selected: boolean;
  error_msg: string;
}

const initialState: JoinProgramState = {
  selected: false,
  error_msg: "",
};

const joinProgramReducer = (state = initialState, action: any) => {
  const error_msg: string = action.payload || "";

  switch (action.type) {
    case TOGGLE_SELECT_TC:
      return {
        ...state,
        selected: !state.selected,
      };
    case TC_FAIL:
      return {
        ...state,
        error_msg: error_msg,
      };
    default:
      return state;
  }
};

export default joinProgramReducer;
