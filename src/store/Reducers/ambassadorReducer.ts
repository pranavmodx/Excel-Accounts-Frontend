import { SET_AMBASSADOR } from "../constants";

export interface AmbassadorState {
  ambassadorId: number;
  freeMembership: number;
  paidMembership: number;
}

const initialState: AmbassadorState = {
  ambassadorId: 0,
  freeMembership: 0,
  paidMembership: 0,
};

const ambassadorReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case SET_AMBASSADOR:
      return {
        ...state,
        ...action.payload,
      };
    default:
      return state;
  }
};

export default ambassadorReducer;
