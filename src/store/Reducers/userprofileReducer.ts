import { SET_PROFILE } from "../constants";

interface IProfile {
  id: number;
  name: string;
  email: string;
  picture: string;
  qrCodeUrl: string;
  institutionName: string;
  gender: string;
  mobileNumber: string;
  category: string;
  institutionId: number | null;
  ambassadorRef: number;
  isPaid: number;
}

const defaultState: IProfile = {
  id: 0,
  name: "",
  email: "",
  picture: "",
  qrCodeUrl: "",
  institutionName: "",
  gender: "Male",
  mobileNumber: "",
  category: "college",
  institutionId: null,
  ambassadorRef: 0,
  isPaid: 0,
};

const userprofileReducer = (state = defaultState, action: any) => {
  switch (action.type) {
    case SET_PROFILE:
      return {
        ...state,
        ...action.payload,
      };
    default:
      return state;
  }
};

export default userprofileReducer;
