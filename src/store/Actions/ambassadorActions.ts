import { SET_AMBASSADOR } from "../constants";
import http from "../../config/http";

export const setAmbassador = (dispatch: any, response: any) => {
  const { ambassadorId, freeMembership, paidMembership } = response;

  dispatch({
    type: SET_AMBASSADOR,
    payload: { ambassadorId, freeMembership, paidMembership },
  });
};

export const signUpForAmbassador = (dispatch: any) => {
  http.get("/Ambassador/signup").then((response) => {
    if (response.response == 'Success') {
      http.get("/Ambassador").then((response) => {
        console.log(response);
        setAmbassador(dispatch, response);
      });
    }
  });
}