import { TOGGLE_SELECT_TC, TC_FAIL } from "../constants";

export const setSelected = (dispatch: any) => {
  dispatch({
    type: TOGGLE_SELECT_TC,
    payload: null,
  });
};

export const setErrorMsg = (dispatch: any, error_msg: string) => {
  dispatch({
    type: TC_FAIL,
    payload: error_msg,
  });
};
