import React, { lazy, Suspense, useEffect } from "react";
import { Route, Switch } from "react-router-dom";
import { useDispatch } from "react-redux";

import Sidebar from "../components/common/Sidebar/Sidebar";
import Navbar from "../components/common/Navbar/Navbar";

import PrivateRoute from "../components/common/PrivateRoute/PrivateRoute";
import CubeSpinner from "../components/common/Loaders/CubeSpinner";
import http from "../config/http";
import { setProfile } from "../store/Actions/userprofileActions";
import { setAmbassador } from "../store/Actions/ambassadorActions";

const Home = lazy(() => import("./Home/Home"));
const UserProfile = lazy(() => import("./UserProfile/UserProfile"));
const CampusAmbassadorPage = lazy(() =>
  import("./CampusAmbassador/CampusAmbassadorPage")
);

const Base = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    http.get("/Profile/view").then((response) => {
      console.log("profile res: ", response);
      setProfile(dispatch, response);
    });
  }, [dispatch]);

  useEffect(() => {
    http.get("/Ambassador").then((response) => {
      console.log(response);
      setAmbassador(dispatch, response);
    });
  }, [dispatch]);

  return (
    <PrivateRoute>
      <div className="App">
        <div className="wrapper ">
          <Sidebar />
          <div className="main-panel">
            <Navbar />
            <div className="content">
              <div className="container-fluid">
                <Suspense fallback={<CubeSpinner />}>
                  <Switch>
                    <Route exact path="/" component={Home} />
                    <Route path="/profile" component={UserProfile} />
                    <Route
                      path="/ambassador-dashboard"
                      component={CampusAmbassadorPage}
                    />
                  </Switch>
                </Suspense>
              </div>
            </div>
          </div>
        </div>
      </div>
    </PrivateRoute>
  );
};

export default Base;
