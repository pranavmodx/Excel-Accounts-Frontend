import React, { useState, useEffect } from "react";
import StatCard from "../../components/AmbassadorDashboard/StatCard";
import StatTable, {
  TableRow,
} from "../../components/AmbassadorDashboard/StatTable";
import Referral from "../../components/AmbassadorDashboard/Referral";

import http from "../../config/http";

import "./AmbassadorDashboard.scss";

const defaultJoinedData: TableRow[] = [
  {
    users: "No referred users",
  },
];

const rankData = [
  {
    rank: 1,
    name: "bunty",
    points: 80,
  },
  {
    rank: 2,
    name: "johnny",
    points: 70,
  },
  {
    rank: 3,
    name: "lenny",
    points: 65,
  },
];

const AmbassadorDashboard = () => {
  const [joinedData, setJoinedData] = useState<TableRow[]>(defaultJoinedData);

  useEffect(() => {
    http.get("/Ambassador/userlist").then((response) => {
      if (response.length !== 0) {
        for (let i = 0; i < response.length; i++) {
          response[i]["S_No"] = i;
        }
        console.log(response);
  
        setJoinedData(response);
      }
    });
  }, []);

  return (
    <>
      <div className="row">
        <Referral />
        <div className="col-md-6 col-lg-6">
          <StatCard
            title={joinedData == defaultJoinedData ? 0 : joinedData.length}
            desc="Joined"
            icon="done_all"
            icon_bg="success"
            tableId="Joined"
          />
          <StatCard
            title={16}
            desc="Points Earned"
            icon="attach_money"
            icon_bg="warning"
            tableId="Rank"
          />
        </div>
      </div>
      <div className="row">
        <StatTable tableData={joinedData} tableTitle="Joined" />
        <StatTable tableData={rankData} tableTitle="Rank" />
      </div>
    </>
  );
};

export default AmbassadorDashboard;
