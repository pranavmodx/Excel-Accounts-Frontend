import React, { lazy, Suspense } from "react";
import DottedLineLoader from "../../components/common/Loaders/DottedLineLoader";
import { useSelector } from "react-redux";

const JoinProgram = lazy(() => import("./JoinProgram"));
const AmbassadorDashboard = lazy(() => import("./AmbassadorDashboard"));

const CampusAmbassadorPage = () => {
  const ambassadorId = useSelector(
    (state: any) => state.ambassador.ambassadorId
  );

  return (
    <Suspense fallback={<DottedLineLoader />}>
      {ambassadorId ? <AmbassadorDashboard /> : <JoinProgram />}
    </Suspense>
  );
};

export default CampusAmbassadorPage;
