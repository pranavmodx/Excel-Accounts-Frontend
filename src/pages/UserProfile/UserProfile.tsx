import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import "./UserProfile.scss";
import { IUpdateProfile } from "./profileTypes";
import { saveProfile } from "../../store/Actions/userprofileActions";

import ViewProfile from "./ViewProfile";
import CreatableSingle from "./CreatableSingle";

const UserProfile = () => {
  const profile = useSelector((state: any) => state.userProfile);

  const dispatch = useDispatch();

  const [updateProfile, setUpdateProfile] = useState<IUpdateProfile>({
    name: "",
    institutionId: 0,
    institutionName: "",
    gender: "",
    mobileNumber: "",
    category: "",
  });
  const [errorMsg, setErrorMsg] = useState<string>("");

  // console.log('updateProfile: ', updateProfile);

  useEffect(() => {
    setUpdateProfile((u:any) => ({
      ...u,
      name: profile.name,
      institutionName: profile.institutionName,
      gender: profile.gender,
      mobileNumber: profile.mobileNumber,
      category: profile.category,
    }));
  }, [
    profile.name,
    profile.institutionName,
    profile.gender,
    profile.mobileNumber,
    profile.category,
  ]);

  const [editable, setEditable] = useState<boolean>(false);
  var opts = { readOnly: !editable, disabled: !editable };

  const handleUpdateProfile = (event: {
    target: { name: any; value: any };
  }) => {
    const { name, value } = event.target;
    setUpdateProfile({ ...updateProfile, [name]: value });
  };
  const setInstitution = (id: number, name: string) => {
    setUpdateProfile({
      ...updateProfile,
      institutionId: id,
      institutionName: name,
    });
  };
  const setInstitutionId = (id: number) => {
    setUpdateProfile({ ...updateProfile, institutionId: id });
  };

  const handleEdit = (event: { preventDefault: () => void }) => {
    setEditable(true);
    opts = { readOnly: false, disabled: false };
    event.preventDefault();
  };

  const submitValue = (event: { preventDefault: () => void }) => {
    event.preventDefault();
    const {
      name,
      gender,
      category,
      mobileNumber,
      institutionName,
    } = updateProfile;
    if (name.trim() && gender && category && mobileNumber.trim()) {
      if (category !== "other" && !institutionName) {
        setErrorMsg("Pls fill out the required fields!");
      } else {
        setEditable(false);
        setErrorMsg("");
        opts = { readOnly: true, disabled: true };

        saveProfile(dispatch, updateProfile);
      }
    } else {
      setErrorMsg("Pls fill out the required fields!");
    }
  };

  return (
    <>
      <div className="content">
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-8">
              <div className="card">
                <div className="card-header card-header-primary">
                  <h4 className="card-title">Edit Profile</h4>
                  <p className="card-category">Complete your profile</p>
                </div>
                <div className="card-body">
                  <form>
                    <div className="row">
                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">Name *</label>
                          <input
                            type="text"
                            className="form-control"
                            name="name"
                            value={updateProfile.name}
                            onChange={handleUpdateProfile}
                            required
                            {...opts}
                            style={{ backgroundColor: "transparent" }}
                          />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">Gender *</label>
                          <select
                            className="form-control"
                            name="gender"
                            value={updateProfile.gender}
                            onChange={handleUpdateProfile}
                            {...opts}
                            style={{ backgroundColor: "transparent" }}
                            required
                          >
                            <option
                              value=""
                              style={{ color: "gray" }}
                              disabled
                              selected
                            >
                              Select
                            </option>
                            <option value="Male" className="text-dark">
                              Male
                            </option>
                            <option value="Female" className="text-dark">
                              Female
                            </option>
                            <option value="Other" className="text-dark">
                              Other
                            </option>
                          </select>
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">
                            Category *
                          </label>
                          <select
                            className="form-control"
                            name="category"
                            value={updateProfile.category}
                            onChange={handleUpdateProfile}
                            {...opts}
                            style={{ backgroundColor: "transparent" }}
                            required
                          >
                            <option
                              value=""
                              style={{ color: "gray" }}
                              disabled
                              selected
                            >
                              Select
                            </option>
                            <option value="college" className="text-dark">
                              College
                            </option>
                            <option value="school" className="text-dark">
                              School
                            </option>
                            <option value="other" className="text-dark">
                              Other
                            </option>
                          </select>
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">
                            Institution Name *
                          </label>
                          <CreatableSingle
                            disabled={
                              !editable || updateProfile.category === "other"
                            }
                            category={updateProfile.category}
                            instName={updateProfile.institutionName}
                            handleInstitutionChange={setInstitution}
                            setInstitutionId={setInstitutionId}
                          />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">
                            Mobile Number *
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            name="mobileNumber"
                            value={updateProfile.mobileNumber}
                            onChange={handleUpdateProfile}
                            maxLength={10}
                            {...opts}
                            style={{ backgroundColor: "transparent" }}
                            required
                          />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">
                            Referral Code
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            name="mobileNumber"
                            value=""
                            maxLength={10}
                            {...opts}
                            style={{ backgroundColor: "transparent" }}
                          />
                        </div>
                      </div>
                    </div>

                    <div className="text-danger">{errorMsg}</div>
                    <button
                      type="submit"
                      onClick={handleEdit}
                      className="btn btn-primary pull-right"
                      disabled={editable}
                    >
                      Edit Profile
                    </button>
                    <button
                      type="submit"
                      onClick={submitValue}
                      className="btn btn-primary pull-right"
                      disabled={!editable}
                    >
                      Update Profile
                    </button>
                    <div className="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <ViewProfile viewProfile={profile} />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default UserProfile;
