import React from "react";

interface IProps {
  children: any;
  modalTitle: string;
  modalId: string;
  saveAction: any;
  disableSave?: boolean;
}

const Modal = ({
  children,
  modalTitle,
  modalId,
  saveAction,
  disableSave,
}: IProps) => {
  return (
    <div
      className="modal fade"
      id={modalId}
      role="dialog"
      aria-labelledby={`${modalId}Label`}
      aria-hidden="true"
    >
      <div className="modal-dialog" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id={`${modalId}Label`}>
              {modalTitle}
            </h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">{children}</div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-secondary"
              data-dismiss="modal"
            >
              Close
            </button>
            <button
              type="button"
              className="btn btn-primary"
              onClick={saveAction}
              data-dismiss="modal"
              disabled={disableSave}
            >
              Save changes
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Modal;
